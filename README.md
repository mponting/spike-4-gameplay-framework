| **Spike ID**        | B4                                                                | **Time Needed** | 1 Day |
|---------------------|-------------------------------------------------------------------|-----------------|-------|
| **Title**           | Base Spike 4 – Gameplay Framework                                 |
| **Personnel**       | Mathew Ponting (blueprogrammer)                                   |
| **Repository Link** | <https://bitbucket.org/blueprogrammer/spike-4-gameplay-framework> |

Introduction
============

In this spike, the user will construct a small unreal game using
knowledge gained from the previous 3 spikes and more. In this spike, we
will create a small collect the boxes game as fast as possible which
includes a score and a timer. In addition, the player controller will
need to have 2 ways of controlling (in this spike, it is a Keyboard and
Player Controller.

Goal
====

Using both Unreal Blueprint and C++, construct a series of actors and a
controllable pawn that together function like a simple game, doesn’t
have to be a good one. IMPORTANT: Both the player pawn and the game
state must be programmed in C++.

TRT (Technology, Resources and Tools)
=====================================

| **Resource**                                                                                                                    | **Needed/Recommended** |
|---------------------------------------------------------------------------------------------------------------------------------|------------------------|
| Variable, Timers and Events - <https://docs.unrealengine.com/latest/INT/Programming/Tutorials/VariablesTimersEvents/index.html> |                        |
| Player Input and Pawns in C++ - <https://docs.unrealengine.com/latest/INT/Programming/Tutorials/PlayerInput/index.html>         |                        |
| Intro to C++ in Unreal - <https://docs.unrealengine.com/latest/INT/Videos/PLZlv_N0_O1gYup-gvJtMsgJqnEB_dGiM4/mSRov77hNR4/>      |                        |

Tasks
=====

Show only the key tasks that will help the developer/reader understand
what is required.

1.  Setup input controls in the Project Settings.

    1.  W and S for Forward Axis (Left Stick Y Axis for Gamepad)

    2.  A and D for Right Axis (Left Stick X Axis for Gamepad)

    3.  Left Shift and Right Trigger for Acceleration.

2.  Using what was discovered in the Player Input and Pawns tutorial,
    create a Pawn using C++ which has functions that controls its
    movement and Speed.

    1.  Create a new variable called speed which is accessed through
        Unreal (Hint: Use UPROPERTY).

    2.  Set up 3 methods that use float values as arguments. Make sure
        to set them to Blueprint Callable (Hint: Use UFUNCTION).

3.  Override the tick method from unreal and using location updates and
    using FVector, update the location of the player object.

4.  In this pawn’s constructor;

    1.  Make AutoPossessPlayer to see if it can get input from Player 0

    2.  Create a UCamera Component, A Box Collider and a Visible
        Component and set them up to your liking. Make sure the Visible
        Component is the Root Component.

    3.  Attach them together so that they stay together in a group.

5.  Back in unreal, create a new Game State CPP template.

    1.  Make 2 new variables called score (int) and
        FreezePlayers (bool).

    2.  In its Constructor, Make Score and FreezePlayers equal to 0 and
        false respectively.

    3.  Create a new method that toggles FreezePlayers between true and
        false, one Gets the FreezePlayers value and one that adds 1 to
        the score variable.

6.  Back in the Player CPP file, create a checker that checks if the
    FreezePlayers variable in the game state is set to true. If it is,
    then make it ignore all movement checking.

7.  In unreal, create a new Blueprint Player Controller and Game Mode.
    Make sure the project has the game mode selected as it’s default
    game mode.

8.  In the player controller in blueprint, in begin play, get reference
    to the current player pawn controlled by player 0 then create events
    for each of the Axis Movments (Forward, Right and Speed).

    1.  Use the Axis Values given in those nodes and make them the
        argument float values and set them to call the movements methods
        mentioned before with the pawn.

9.  In the Game Mode, set up Default Player Pawn, The Default Game State
    and The Default Player Controller to each of the created classes. In
    addition, Make it print out a string on to the screen with the
    player’s current score.

10. Setup a score blueprint that on contact with the player, will
    destroy it add 1 to the score.

11. In the game mode, create a checker that checks for all score actors
    if they exist. If they don’t; toggle freeze for player and end
    the game.

12. Customize the game level to your liking. Place as many score object
    on the map as you want.

Discovered
==========

-   You can make C++ classes be a base of a blueprint class but it
    cannot be the other way round.

-   Using UFUNCTION and UPROPERTY allow blueprint and C++ to communicate
    with each other.

-   You can use blueprint and C++ in the same project. Its recommended
    to use blueprint most times but when diving into much more
    complicated stuff with heavy mathematics, C++ is the way to go.

-   PlayerController stores when the buttons and keys are pressed on
    a controller/keyboard/etc.

-   The Game State is the state of the world that is being played.

-   Game Mode holds all the base information about the game such as the
    default player pawn, the default pawn, etc.

Open Issues and Risks
=====================

Issues
------

-   When you quit out of unreal while Visual Studio is open and you open
    the unreal project, you may sometimes have to reload Visual Studio.


Recommendations
===============

-   This link is recommended if you want to have a understanding on how
    blueprint and C++ work together and how use C++ classes as a base
    for blueprint classes.  
    <https://docs.unrealengine.com/latest/INT/Gameplay/ClassCreation/CodeAndBlueprints/index.html>

-   In Blueprint, Group nodes together and try to avoid using common
    nodes between 2 events as it can look untidy.

-   There is a lot more to do with Collisions, if you need to make an
    object that collides with walls, I recommend building the base
    character in C++ with the base need components needed already
    present them then make a blueprint class inheriting the C++ class.

    -   If I was to redo this spike again (if I had more time), I would
        recommend looking into using alternative methods that allows
        collisions to be handled much more easier than the method
        completed in this spike.